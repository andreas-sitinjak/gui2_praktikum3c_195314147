package GUI_3C;

import java.awt.Color;
import java.awt.Container;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;

public class Latihan2 extends JFrame {

    private static final int FRAME_WIDTH = 300;
    private static final int FRAME_HEIGHT = 200;
    private static final int FRAME_X_ORIGIN = 150;
    private static final int FRAME_Y_ORIGIN = 250;
    private static final int BUTTON_WIDTH = 80;
    private static final int BUTTON_HEIGHT = 40;

    public static void main(String[] args) {
        Latihan2 frame = new Latihan2();
        frame.setVisible(true);
    }

    public Latihan2() {
        Container contentpane = getContentPane();
        JButton button1, button2, button3;

        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setResizable(true);
        setTitle("Button Test");
        setLocation(FRAME_X_ORIGIN, FRAME_Y_ORIGIN);
        contentpane.setLayout(null);
        contentpane.setBackground(Color.white);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        button1 = new JButton("Yeloow");
        button1.setBounds(120, 25, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentpane.add(button1);

        button2 = new JButton("Blue");
        button2.setBounds(230, 25, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentpane.add(button2);

        button3 = new JButton("red");
        button3.setBounds(340, 25, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentpane.add(button3);
    }
}
