package GUI_3C;

import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;

public class Latihan3 extends JDialog {

    private JLabel label_image;
    private JLabel label_text;

    public Latihan3() {

        URL gambar = this.getClass().getResource("IMG-20190306-WA0008.jpg");
        this.setSize(300, 150);
        this.setVisible(true);
        this.setTitle("Text & Icon Label");

        ImageIcon image = new ImageIcon(gambar);
        label_image = new JLabel();
        label_image.setIcon(image);
        this.add(label_image);
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

    }

    public static void main(String[] args) {
        new Latihan3();
    }
}
