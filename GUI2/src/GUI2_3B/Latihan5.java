package GUI2_3B;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class Latihan5 extends JFrame {

    private static final int FRAME_WIDTH = 300;
    private static final int FRAME_HEIGHT = 200;
    private static final int FRAME_X_ORIGIN = 150;
    private static final int FRAME_Y_ORIGIN = 250;
    private JList list;

    public static void main(String[] args) {
        Latihan5 frame = new Latihan5();
        frame.setVisible(true);
    }

    public Latihan5() {
        Container contentpane;
        JPanel listPanel, okPanel;

        JButton okButton;
        String[] names = {"Ape", "Bat", "Bee", "Cat",
            "Dog", "Eel", "Fox", "Gnu",
            "Hen", "Man", "Sow", "Yak"};

        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setTitle("program Latihan 5");
        setLocation(FRAME_X_ORIGIN, FRAME_Y_ORIGIN);

        contentpane = getContentPane();
        contentpane.setBackground(Color.white);
        contentpane.setLayout(new BorderLayout());

        listPanel = new JPanel(new GridLayout(0, 1));
        listPanel.setBorder(BorderFactory.createTitledBorder("There-Leter Animal Names"));

        list = new JList(names);
        listPanel.add(new JScrollPane(list));

        okPanel = new JPanel(new FlowLayout());
        okButton = new JButton("OK");
        okPanel.add(okButton);

        contentpane.add(listPanel, BorderLayout.CENTER);
        contentpane.add(okPanel, BorderLayout.SOUTH);

        setDefaultCloseOperation(EXIT_ON_CLOSE);

    }
}
