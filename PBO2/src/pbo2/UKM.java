package pbo2;

public class UKM {

    private String namaUnit;
    private Mahasiswa ketua;
    private Mahasiswa sekretaris;
    private penduduk[] anggota;

    public UKM() {
    }

    public UKM(String namaUnit) {
        this.namaUnit = namaUnit;
    }

    public void setnamaUnit(String namaUnit) {
        this.namaUnit = namaUnit;
    }

    public String getnamaUnit() {
        return namaUnit;
    }

    public void setketua(Mahasiswa ketua) {
        this.ketua = ketua;
    }

    public Mahasiswa getketua() {
        return ketua;
    }

    public void setsekretaris(Mahasiswa sekretaris) {
        this.sekretaris = sekretaris;
    }

    public Mahasiswa getsekretaris() {
        return sekretaris;
    }

    public void setanggota(penduduk[] anggota) {
        this.anggota=anggota;
    }

    public penduduk[] getanggota() {
        return anggota;
    }
}
