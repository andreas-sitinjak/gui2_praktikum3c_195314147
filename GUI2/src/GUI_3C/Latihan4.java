package GUI_3C;

import java.awt.Color;
import java.awt.Container;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JTextArea;

public class Latihan4 extends JDialog {

    private static final int FRAME_WIDTH = 300;
    private static final int FRAME_HEIGHT = 200;
    private static final int FRAME_X_ORIGIN = 150;
    private static final int FRAME_Y_ORIGIN = 250;
    private static final int BUTTON_WIDTH = 80;
    private static final int BUTTON_HEIGHT = 40;

    public static void main(String[] args) {
        Latihan4 frame = new Latihan4();
        frame.setVisible(true);
    }

    public Latihan4() {
        Container contentpane = getContentPane();
        JButton OKButton;
        JButton CancelButton;

        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setResizable(true);
        setTitle("CheckBoxDemo");
        setLocation(FRAME_X_ORIGIN, FRAME_Y_ORIGIN);
        contentpane.setLayout(null);
        contentpane.setBackground(Color.white);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        JTextArea text = new JTextArea("Welcome to Java");
        text.setBounds(170, 40, 150, 50);
        contentpane.add(text);

        JCheckBox box = new JCheckBox("Centered");
        box.setBounds(400, 5, 100, 21);
        contentpane.add(box);

        JCheckBox box1 = new JCheckBox("Bold");
        box1.setBounds(400, 40, 100, 21);
        contentpane.add(box1);

        JCheckBox box2 = new JCheckBox("Italic");
        box2.setBounds(400, 75, 150, 21);
        contentpane.add(box2);

        OKButton = new JButton("Left");
        OKButton.setBounds(160, 100, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentpane.add(OKButton);

        CancelButton = new JButton("Right");
        CancelButton.setBounds(260, 100, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentpane.add(CancelButton);
    }

}
