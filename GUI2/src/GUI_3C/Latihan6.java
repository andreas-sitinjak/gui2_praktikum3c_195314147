package GUI_3C;

import java.awt.Container;
import java.awt.GridLayout;
import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class Latihan6 extends JDialog {

    private final JComboBox box;
    private final JLabel image, text;
    private final JList list;
    private final JPanel listPanel;
    private static final int FRAME_WIDTH = 600;
    private static final int FRAME_HEIGHT = 350;
    private static final int BUTTON_WIDTH = 584;
    private static final int BUTTON_HEIGHT = 40;

    public static void main(String[] args) {
        Latihan6 dialog = new Latihan6();
        dialog.setVisible(true);
    }

    public Latihan6() {
        String[] names = {"INDONESIA",
            " disebut juga dengan Republik Indonesia (RI) ",
            " ",
            "Indonesia adalah negara di Asia Tenggara yang dilintasi garis",
            "khatulistiwa dan berada diantara daratan benua Asia dan Australia,",
            "serta antara Samudra Pasifik dan Samudra Hindia.", "Indonesia adalah",
            "negara kepulauan terbesar di dunia yang terdiri dari 17.504 pulau.",
            "Nama alternatif yang biasa dipakai adalah Nusantara.", "Dengan populasi",
            "Hampir 270.054.853 jiwa pada tahun 2018",
            "Indonesia adalah negara berpenduduk terbesar keempat di dunia",
            "an negara yang berpenduduk Muslim terbesar di dunia,",
            "dengan lebih dari 230 juta jiwa."};
        Container contentPane = getContentPane();
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setResizable(true);
        setTitle("ComboBoxDemo");
        contentPane.setLayout(null);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        URL ii = this.getClass().getResource("indo.jpg");
        ImageIcon img = new ImageIcon(ii);
        image = new JLabel();
        image.setIcon(img);
        image.setBounds(80,50,200,150);
        contentPane.add(image);
        box = new JComboBox();
        box.addItem("INDONESIA");
        box.setBounds(1, 1, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(box);
        text = new JLabel("INDONESIA");
        text.setBounds(95, 110, 110, 310);
        contentPane.add(text);
        listPanel = new JPanel(new GridLayout(0, 1));
        list = new JList(names);
        listPanel.add(new JScrollPane(list));
        listPanel.setBounds(260, 60, 300, 230);
        contentPane.add(listPanel);
    }

}
