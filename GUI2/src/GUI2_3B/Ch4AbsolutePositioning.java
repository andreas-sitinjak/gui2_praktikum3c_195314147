package GUI2_3B;

import java.awt.Color;
import java.awt.Container;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

public class Ch4AbsolutePositioning extends JFrame {

    private static final int FRAME_WIDTH = 300;
    private static final int FRAME_HEIGHT = 200;
    private static final int FRAME_X_ORIGIN = 300;
    private static final int FRAME_Y_ORIGIN = 100;
    private static final int BUTTON_WIDTH = 80;
    private static final int BUTTON_HEIGHT = 40;
    private JButton cancelButton;
    private JButton okButton;
    private JTextField textfield;

    public static void main(String[] args) {
        Ch4AbsolutePositioning frame = new Ch4AbsolutePositioning();
        frame.setVisible(true);
    }

    public Ch4AbsolutePositioning() {
        Container contentpane = getContentPane();
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setResizable(true);
        setTitle("Program Ch4AbsolutePositioning");
        setLocation(FRAME_X_ORIGIN, FRAME_Y_ORIGIN);

        contentpane.setLayout(null);
        contentpane.setBackground(Color.white);

        okButton = new JButton("OK");
        okButton.setBounds(150, 100, BUTTON_WIDTH, BUTTON_WIDTH);
        contentpane.add(okButton);
        
        cancelButton = new JButton("CANCEL");
        cancelButton.setBounds(250, 100, BUTTON_WIDTH, BUTTON_WIDTH);
        contentpane.add(cancelButton);

        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
}
