package GUI2_3A;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Latihan3 extends JFrame {

    public Latihan3() {
        this.setSize(300, 400);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Ini Class Turunan dari class JFrame");
        this.setVisible(true);

        JPanel panel = new JPanel();
        JButton tombol = new JButton();
        tombol.setText("Ini Tombol");
        panel.add(tombol);
        this.add(panel);

        JLabel label = new JLabel();
        JButton masuk = new JButton();
        masuk.setText("Masukan");
        panel.add(masuk);
        this.add(panel);
    }

    public static void main(String[] args) {
        new Latihan3();
    }

}
