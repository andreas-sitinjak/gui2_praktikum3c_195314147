package GUI_3C;

import java.awt.Container;
import java.awt.GridLayout;
import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class Latihan7 extends JDialog {

    private final JList list;
    private final JPanel listPanel;
    private JLabel image;
    private static final int FRAME_WIDTH = 800;
    private static final int FRAME_HEIGHT = 500;

    public Latihan7() {
        String[] names = {"Kanada", "Jerman", "Indonesia", "Malaysia", "Jepang", "Spanyol", "Vietnam", "Italy",
             "Portugal", "Kameron", "Inggris"};
        Container contentPane = getContentPane();
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setResizable(true);
        setTitle("ListDemo");
        contentPane.setLayout(null);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        URL A = this.getClass().getResource("Indonesia.jpg");
        ImageIcon a = new ImageIcon(A);
        image = new JLabel();
        image.setIcon(a);
        image.setBounds(110, 10, 200, 80);
        contentPane.add(image);
        URL B = this.getClass().getResource("inggris.jpg");
        ImageIcon b = new ImageIcon(B);
        image = new JLabel();
        image.setIcon(b);
        image.setBounds(220, 10, 200, 80);
        contentPane.add(image);
        URL C = this.getClass().getResource("italia.jpg");
        ImageIcon c = new ImageIcon(C);
        image = new JLabel();
        image.setIcon(c);
        image.setBounds(400, 10, 200, 80);
        contentPane.add(image);
        URL D = this.getClass().getResource("kanada.jpg");
        ImageIcon d = new ImageIcon(D);
        image = new JLabel();
        image.setIcon(d);
        image.setBounds(600, 10, 200, 100);
        contentPane.add(image);
        URL E = this.getClass().getResource("vietnam.jpg");
        ImageIcon e = new ImageIcon(E);
        image = new JLabel();
        image.setIcon(e);
        image.setBounds(110, 120, 200, 80);
        contentPane.add(image);
        URL F = this.getClass().getResource("jepang.jpg");
        ImageIcon f = new ImageIcon(F);
        image = new JLabel();
        image.setIcon(f);
        image.setBounds(290, 120, 200, 80);
        contentPane.add(image);
        URL G = this.getClass().getResource("malaysia.jpg");
        ImageIcon g = new ImageIcon(G);
        image = new JLabel();
        image.setIcon(g);
        image.setBounds(480, 120, 200, 80);
        contentPane.add(image);
        URL H = this.getClass().getResource("spanyol.jpg");
        ImageIcon h = new ImageIcon(H);
        image = new JLabel();
        image.setIcon(h);
        image.setBounds(110, 220, 200, 160);
        contentPane.add(image);
        URL I = this.getClass().getResource("jerman.jpg");
        ImageIcon i = new ImageIcon(I);
        image = new JLabel();
        image.setIcon(i);
        image.setBounds(290, 220, 200, 160);
        contentPane.add(image);
        URL J = this.getClass().getResource("portugal.jpg");
        ImageIcon j = new ImageIcon(J);
        image = new JLabel();
        image.setIcon(j);
        image.setBounds(420, 220, 200, 160);
        contentPane.add(image);
        
        URL K = this.getClass().getResource("spanyol.jpg");
        ImageIcon k = new ImageIcon(K);
        image = new JLabel();
        image.setIcon(k);
        image.setBounds(110, 220, 200, 160);
        contentPane.add(image);
        
        listPanel = new JPanel(new GridLayout(0, 1));
        list = new JList(names);
        listPanel.add(new JScrollPane(list));
        listPanel.setBounds(1, 1, 100, 220);
        contentPane.add(listPanel);
    }

    public static void main(String[] args) {
        Latihan7 dialog = new Latihan7();
        dialog.setVisible(true);
    }
}
