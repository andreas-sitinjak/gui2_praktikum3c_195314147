package GUI_3C;

import java.awt.Color;
import java.awt.Container;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;

public class Latihan1 extends JFrame {

    private JMenuBar menuBar;
    private JMenu menu_File;
    private JMenu menu_Edit;

    public Latihan1() {
        Container content = getContentPane();
        menuBar = new JMenuBar();
        menu_File = new JMenu("File");
        menu_Edit = new JMenu("Edit");
        menuBar.add(menu_File);
        menuBar.add(menu_Edit);
        this.setJMenuBar(menuBar);
        this.setTitle("                                Frame Pertama");
        this.setSize(400, 400);
        content.setBackground(Color.pink);
    }

    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {

                Latihan1 main = new Latihan1();
                main.setVisible(true);
                main.setResizable(false);
                main.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            }
        });
    }
}
