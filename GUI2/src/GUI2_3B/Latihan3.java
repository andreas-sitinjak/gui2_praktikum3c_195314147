package GUI2_3B;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import javax.swing.JButton;
import javax.swing.JFrame;

public class Latihan3 extends JFrame {

    private static final int FRAME_WIDTH = 300;
    private static final int FRAME_HEIGHT = 200;
    private static final int FRAME_X_ORIGIN = 150;
    private static final int FRAME_Y_ORIGIN = 250;

    public static void main(String[] args) {
        Latihan3 frame = new Latihan3();
        frame.setVisible(true);
    }

    public Latihan3() {
        Container contentpane;
        JButton button1, button2, button3, button4, button5;

        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setTitle("Program Latihan 3 ");
        setLocation(FRAME_X_ORIGIN, FRAME_Y_ORIGIN);

        contentpane = getContentPane();
        contentpane.setBackground(Color.WHITE);
        contentpane.setLayout(new BorderLayout());

        button1 = new JButton("button 1");
        button2 = new JButton("button 2");
        button3 = new JButton("button 3");
        button4 = new JButton("button 4");
        //button5 = new JButton("button 5");
        
        contentpane.add(button1, BorderLayout.NORTH);
        contentpane.add(button2, BorderLayout.SOUTH);
        contentpane.add(button3, BorderLayout.EAST);
        contentpane.add(button4, BorderLayout.WEST);
        //contentpane.add(button5, BorderLayout.CENTER);

        setDefaultCloseOperation(EXIT_ON_CLOSE);

    }
}
