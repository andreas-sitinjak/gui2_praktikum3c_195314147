package GUI2_3B;

import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class Latihan1C extends JFrame{

    private static final int FRAME_WIDTH = 350;
    private static final int FRAME_HEIGHT = 300;
    private static final int FRAME_X_ORIGIN = 300;
    private static final int FRAME_Y_ORIGIN = 100;
    private static final int BUTTON_WIDTH = 80;
    private static final int BUTTON_HEIGHT = 40;
    private JButton cancelButton;
    private JButton okButton;
    private JTextField txtField;

    public static void main(String[] args) {
        Latihan1C frame = new Latihan1C();
        frame.setVisible(true);
    }

    public Latihan1C() {
        Container contentPane = getContentPane();
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setResizable(true);
        setTitle("Input Data");
        setLocation(FRAME_X_ORIGIN, FRAME_Y_ORIGIN);
        contentPane.setLayout(null);
        contentPane.setBackground(Color.red);

        JLabel label = new JLabel("Nama :");
        label.setBounds(11, 3, 41, 22);
        contentPane.add(label);

        JLabel label1 = new JLabel("Jenis Kelamin :");
        label1.setBounds(11, 31, 101, 22);
        contentPane.add(label1);

        JLabel label2 = new JLabel("Hobi :");
        label2.setBounds(11, 61, 41, 22);
        contentPane.add(label2);

        JTextField text = new JTextField(null);
        text.setBounds(98, 3, 151, 22);
        contentPane.add(text);

        JRadioButton radio = new JRadioButton("Laki-Laki");
        radio.setBounds(98, 31, 101, 22);
        contentPane.add(radio);

        JRadioButton radio2 = new JRadioButton("Perempuan");
        radio2.setBounds(198, 31, 101, 22);
        contentPane.add(radio2);

        JCheckBox box = new JCheckBox("Olahraga");
        box.setBounds(98, 61, 101, 22);
        contentPane.add(box);

        JCheckBox box1 = new JCheckBox("Shopping");
        box1.setBounds(98, 86, 101, 22);
        contentPane.add(box1);

        JCheckBox box2 = new JCheckBox("Computer Games");
        box2.setBounds(98, 111, 151, 22);
        contentPane.add(box2);

        JCheckBox box3 = new JCheckBox("Nonton Bioskop");
        box3.setBounds(98, 136, 151, 22);
        contentPane.add(box3);

        okButton = new JButton("Ok");
        okButton.setBounds(70, 190, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(okButton);

        cancelButton = new JButton("Cancel");
        cancelButton.setBounds(190, 190, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(cancelButton);

        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
}


